import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const req = ctx.getRequest<Request>();
    const res = ctx.getResponse<Response>();
    const status = exception.getStatus();

    const message = {
      statusCode: status,
      timestamp: new Date().toISOString(),
      method: req.method,
      path: req.url,
      response: exception.getResponse()
    };

    console.log(message);
    res.status(status).json(message);
  }
}
