import { NestFactory } from '@nestjs/core';
import * as csurf from 'csurf';
import * as helmet from 'helmet';

import { AppModule } from './app.module';

import { HttpExceptionFilter } from './filters/http-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(helmet());
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(+process.env.PORT || 3000);
  app.use(csurf());
}
bootstrap();
