import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { AuthModule } from '../auth/auth.module';

import { AuthMiddleware } from './middlewares/auth.middleware';

import { ApiController } from './controllers/api.controller';
import { ApiService } from './services/api.service';
import { GeojsonService } from './services/geojson/geojson.service';

@Module({
  controllers: [ApiController],
  imports: [AuthModule],
  providers: [ApiService, GeojsonService]
})
export class ApiModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(ApiController);
  }
}
