export class JwtAccessToken {
  accessToken: string;
  expiresIn: number;
}
